## Front End Best Practices

This is for the front end of EXA - specifically functions that are served to the client

1.  #### Always Use Internationalized Text

    1.  Exa is an international application and thus requires i18n everywhere.
    2.  When writing a label in HTML use the attributes `i18n` for inner HTML, `i18nt` for titles, or `i18np` for placeholders.
    3.  In front end JavaScript files use `commonjs.geti18NString('file.section.string')`
    4.  commonjs.showWarning() natively supports i18n. Always use it.
    5.  Adding New i18n Strings
        1.  Newly defined i18n strings should only ever be added to `i18n/default` *unless* you actually understand that language and can contribute a contextually accurate translation.
        2.  Never add English strings to non-english i18n files. It messes with automated translations and can be difficult to find and correct.

    > **Bad**:  `<p>Hello</p>` 
    >
    > **Good**: `<p i18n="file.section.hello"></p>`
    >
    > **Bad**:  `commonjs.showWarning("Select File to Process");`
    >
    > **Good**: `commonjs.showWarning('messages.warning.patient.selectfiletoprocess');`