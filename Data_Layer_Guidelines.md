## Data Layer Best Practices

This is for the back end of EXA - specifically functions that directly talk to the database

1.  #### Prefer`const`, occasionally use `let`, **never** use `var`

    1.  There is never a good reason to use var on the back end of EXA.
    2.  `let` and `const` should be the way we declare variables on the backend.
    3.  Objects should _always_ be declared with `const` because you can write new properties onto the object without mutating it.

    > **Bad**: `var someObj = {}`
    >
    > **Bad**: `let someObj = {}`
    >
    > **Good**: `const someObj = {}`

2.  #### Prefer Arrow Functions for Callbacks

    1.  Opt for arrow function syntax for _all_ callback functions except in rare occasions that the `this` binding specifically calls for the use of regular Functions

    > **Bad**: `helper.query(arg1, function(err, result){ callback(err, result) })`
    >
    > **OK**: `helper.query(arg1, (err, result}) => callback(err, result.rows))`
    >
    > **Good**: `helper.query(arg1, (err, {rows}) => callback(err, rows))` >
    >
    > **Good**: `helper.query(arg1, callback)`

3.  #### Prefer Regular Functions for Methods

    1.  This is already happening in most of EXA. This will ensure that the expected `this` context will be there when needed.

4.  #### Destructure required values for every function

    1.  Destructuring makes code simpler and easier to read
    2.  documents functions by having all the variables exist by themselves.
    3.  Destructuring should occur within the function body.
    4.  DO NOT destructure unused variables. This defeats some of the purpose of destructuring clarity.

5.  #### Always Define SQL Queries as variables

    1.  By pulling out all query statements into variables - this makes the functions substantially easier to read.

6.  #### Prefer using `sql-template-strings`

7.  #### Always use strict equality

    > **Bad**: `if(someVar == "some string"){...}`
    >
    > **Good**: `if(someVar === "some string"){...}`

8.  #### Convert Boolean "string" values to Booleans

    > **Bad**: `if(someArg === "true"){...}`
    >
    > **Good**:
    >
    >       someArg = (someArg === "true") // cast to Boolean
    >       if(someArg){...}

9.  #### Always return callbacks

    1.  V8 can do a number of performance optimizations
    2.  ensures a callback never gets called more than once.
    3.  When using arrow functions, you can immediately return a value by excluding the brackets without the use of the return keyword.

10. #### All new functions should be documented with [JSDoc]('http://usejsdoc.org/').

    1.  This will help further document our codebase and make development easier, faster, and less prone to bugs. Most important document we want [@param](http://usejsdoc.org/tags-param.html)

11. #### Always refactor and remove Async.io

    1.  All instances of the async package should be removed as they are found. If there is specific need for flow control, functions should return native JavaScript promises and async/await syntax can provide clear and concise flow control for the function.

12. #### Avoid large if/else blocks

    1.  We should strive to keep the logical flow of the application obvious at every point.
    2.  Many functions currently use large nested if/else blocks that call different logical patterns whenever certain conditions are encountered.
    3.  We should keep this to a minimum, pull out functions that perform logical operations into their own helpers.

13. #### Use Verbose and correct function and variable names with correct spelling
    1.  All function and variable names should be clear exactly what we expect the function to do and what information the variable should contain.
    2.  Variables should also be spelled correctly.
    3.  All JavaScript variables should be camelCased.
    4.  SQL table and column names should be snake_cased.
    5.  Booleans should be prefixed with 'is' or 'has.' ie... isActive or hasPatients
    6.  Arrays should always be plural and imply many
    7.  Objects should imply many and also what data they are representing
        > **Bad**: `args`
        >
        > **Good**:`finalIncomingPatientSaveData`.

Below is a small example of how many back end functions should look after you touch them.

```JavaScript
const SQL = require('sql-template-strings');
{...
    /**
     * functionName is an example of a good function
     * @param {object} functionArgs - incoming data
     * @param {string} functionArgs.arg1 - some string we need
     * @param {string} functionArgs.arg3.arg4 - some data we need to filter on
     */
    functionName: function (functionArgs, callback){
        // destructure all needed variables for the function.
        const {
            hasArg1,
            arg3: {
                arg4,
            } = {}
        } = functionArgs

        // Always declare sql queries as variables. This unties the query from the function.
        // It makes the string easier to read as it stands by itself.
        // We also use sql-template-strings to help parameterize the query and
        // further make it easier to read and comprehend.
        // We can always use const when using sql-template-strings since sql is an object

        const sql = SQL`SELECT some_row FROM some_table WHERE some_data = ${arg4}`;

        // We always use strict equality

        if(hasArg1 === "iLoveStrictEquality"){
          sql.append(SQL`AND some_other_data = ${hasArg1}`)
        }

        // we can generally condense our db call to one easy to read line
        // notice we use an arrow function as a callback and destructure the rows
        // property. This makes for a terse but easy to understand syntax.
        // We should always avoid adding more logic within the callback.
        // The arrow functionsreturns the callback

        helper.query(sql.text, sql.params, (err, {rows}) => callback(err, rows));
    },...
}
```
